$(function() {

	var swiper2 = new Swiper('.swiper-2', {
		navigation: {
			nextEl: '.sbn2',
			prevEl: '.sbp2',
		},
		slidesPerView: 4,
		loop: true,
		pagination: {
			el: '.swiper-pagination-2',
		},
		breakpoints: {
			768: {
				slidesPerView: 1,
				spaceBetween: 100
			}
		}
	});

	var swiper3 = new Swiper('.swiper-3', {
		navigation: {
			nextEl: '.sbn3',
			prevEl: '.sbp3',
		},
		slidesPerView: 1,
		loop: true,
		pagination: {
			el: '.swiper-pagination-3',
		},
		breakpoints: {
			768: {
				slidesPerView: 1,
				spaceBetween: 100
			}
		}
	});

	var swiper1 = new Swiper('.swiper-1', {
		navigation: {
			nextEl: '.sbn1',
			prevEl: '.sbp1',
		},
		slidesPerView: 3,
		loop: true,
		pagination: {
			el: '.swiper-pagination-1',
		},
		breakpoints: {
			768: {
				slidesPerView: 1,
				spaceBetween: 40
			}
		}
	});

	AOS.init();

	var speed = 'slow';

	$('html, body').hide();

	$(document).ready(function() {
		$('html, body').fadeIn(speed, function() {
			$('a[href], button[href]').click(function(event) {
				var url = $(this).attr('href');
				if (url.indexOf('#') == 0 || url.indexOf('javascript:') == 0) return;
				event.preventDefault();
				$('html, body').fadeOut(speed, function() {
					window.location = url;
				});
			});
		});
	});


	$('.top-line-center span').on('click', function()  {
		$('.select-form').slideToggle('400');
		$('.top-line-center span .fa').toggleClass('arrows-active');
	});

	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});

	$(".filter-button").click(function(){
		var value = $(this).attr('data-filter');

		if(value == "all")
		{
			//$('.filter').removeClass('hidden');
			$('.filter').show('1000');
		}
		else
		{
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
			$(".filter").not('.'+value).hide('3000');
			$('.filter').filter('.'+value).show('3000');

		}
	});

	if ($(".filter-button").removeClass("active")) {
		$(this).removeClass("active");
	}
	$(this).addClass("active");




});

